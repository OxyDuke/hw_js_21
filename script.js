document.addEventListener("DOMContentLoaded", function() {
  const drawCircleButton = document.getElementById("drawCircleButton");
  const inputContainer = document.getElementById("inputContainer");
  const circleContainer = document.getElementById("circleContainer");

  drawCircleButton.addEventListener("click", () => {
    drawCircleButton.style.display = "none";
    inputContainer.innerHTML = `
      <label for="diameter">Введіть діаметр кола:</label>
      <input type="number" id="diameter" min="10" max="100">
      <button id="createCircleButton">Намалювати</button>
    `;

    const createCircleButton = document.getElementById("createCircleButton");

    createCircleButton.addEventListener("click", () => {
      const diameter = document.getElementById("diameter").value;

      if (diameter >= 10 && diameter <= 100) {
        createRandomCircles(diameter);

        createCircleButton.style.display = "none";
      } else {
        alert("Діаметр має бути від 10 до 100");
      }
    });
  });

  function createRandomCircles(diameter) {
    circleContainer.innerHTML = "";

    const circleCount = 100;
    let circleDivs = [];

    for (let i = 0; i < circleCount; i++) {
      const circle = document.createElement("div");
      circle.classList.add("circle");
      circle.style.width = `${diameter}px`;
      circle.style.height = `${diameter}px`;
      circle.style.backgroundColor = getRandomColor();

      circleDivs.push(circle);
    }

    // Додаємо всі кола на сторінку одним махом
    circleContainer.append(...circleDivs);

    // Встановлюємо обробник події для всього контейнера
    circleContainer.addEventListener("click", (event) => {
      const clickedCircle = event.target;
      if (clickedCircle.classList.contains("circle")) {
        const index = circleDivs.indexOf(clickedCircle);
        if (index !== -1 && circleDivs.length > 1) {
          // Видаляємо клікнуте коло
          clickedCircle.style.display = "none";
          // Додаємо нове коло зліва
          const newCircle = circleDivs.pop();
          newCircle.style.display = "inline-block";
        }
      }
    });
  }

  function getRandomColor() {
    const letters = "0123456789ABCDEF";
    let color = "#";
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
});
